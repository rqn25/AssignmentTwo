
import java.awt.TextField;
import java.io.IOException;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;

public class SignupController {
	@FXML
	TextField uname;
	@FXML
	PasswordField pw;
	@FXML
	PasswordField confirmpw;

public void createAccount() {
	if (pw.getText().equals(confirmpw.getText())) {
	User user = new User(null, null, null, null, uname.getText(), null, null, pw.getText(), null);
	UserDB.getUsers().add(user);
	try {
	UserIO.writeUsers(UserDB.getUsers());
	} catch (IOException e) {
	System.err.println("Cannot write UserDB to binary file.");
	e.printStackTrace();
	}
	}

	System.out.println(UserDB.getUsers());

	}

}
