
import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class SignupJavaFXView {

public SignupJavaFXView() throws IOException {
	Stage signupStage = new Stage();
	Parent signupView = FXMLLoader.load(getClass().getResource("SignupJavaFx.fxml"));
	signupStage.setTitle("Signup Page");
	signupStage.setScene(new Scene(signupView, 300, 275));
	signupStage.show();

	}

}
