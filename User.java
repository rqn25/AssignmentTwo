
public class User {
	private String userName;
	private String email;
	private String phone;
	private String password;
	private String photo;

public User(String firstName, String lastName, String dob, String gender, String userName, String email, String phone, String password, String photo) {
	this.userName = userName;
	this.email = email;
	this.phone = phone;
	this.password = password;
	this.photo = photo;

	}

public String getUserName() {
	return userName;

	}

public String getEmail() {
	return email;

	}

public String getPhone() {
	return phone;

	}

public String getPassword() {
	return password;

	}

public String getPhoto() {
	return photo;

	}

}
